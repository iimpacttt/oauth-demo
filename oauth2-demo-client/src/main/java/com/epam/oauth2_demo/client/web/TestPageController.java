package com.epam.oauth2_demo.client.web;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestPageController {

    @GetMapping("/securedPage")
    public String securedPage (Model model){
        model.addAttribute("authentication" , SecurityContextHolder.getContext().getAuthentication());
        return "securedPage";
    }
}
